# Data Platform Hiring Assignment

## Problem Statement

Develop a generic framework for periodic data extraction from Postgres or MySQL.

## Details

- Tables to be extracted: Multiple (minimum 2)
- Tables should have columns to find incremental data: any timestamp based columns, (ex: created_time, updated_time)
- Selection of columns should be configurable
- Export frequency should be configurable (ex: hourly)
- Time Range: Configurable (ex: 12 hours == fetches data for the last 12 hours, for example if the job is triggered at 9AM, then it should fetch the data from yesterday 9PM to today 9AM - 12hrs)
- Output Format: csv
- Delimiter: Configurable
- Compression: gz
- Output Storage: Local files


## Must have

- Externalised configuration, to support multiple tables
- Support for sliding window based extractions

Example: Bootstrap a table to extract last 12 hrs data at 8am (8pm to 8am), set at hourly frequency. Next time the job runs for the table, it should pick up data from (9pm to 9am, if 9am failed, then in the next trigger, it should export 9PM to 10AM - 13hrs).

## Addidtional Information

- Please assume some of the columns have JSON in it, which needs to be split into multiple keys as per the need. The pipeline should have a transformation layer to handle such scenario (ex column: name_data, sample value: {"firstname": "aaa","lastname":"bbb"})
- CDC implementation (debezium, maxwell etc) is not required. The framework should query the source db and extract the data from it.

## Nice to have:

- Auto-recovery - If an instance of extraction fails, including the failed window when it runs next
- Python is preferred, but any language can be used 

## Submission

Create a git repo on gitlab or github, and share the link of the code base. Please include the DDL's along with the source code that is used for the extraction

## Questions
For any questions, please contact tanmay.k@kinaracapital.com
