## DevOps Hiring Project

This is to check your proficiency in DevOps operations. We define DevOps as being proficient in application versioning, CI/CD pipelines, Docker containers, Application Performance Monitoring (APM), cloud and documentation. It is expected to have some knowldge of Application Security, atleast comfortable in fixing vulnerabilities. 

## Problem

You're expected to build a CI/CD pipeline instructions of continous update of a wordpress website for upgrading the WP version. use tagged Wordpress's core [release files](https://github.com/WordPress/WordPress/releases) from Wordpress git repository. Use existing custom WP theme and plugins from your own seperate repository. You  need to set up pipeline, building docker image, yml files and deploying it on container. Plugin update and Best practise  of development is expected to be followed. Any additional feature is a plus

## Submission

Create a git repo on gitlab or github, whichever you are more comfortable with, and share the link of the code base.

## Questions
For any questions, please contact nayankumar.v@kinaracapital.com

## Bonus Points

+ 5% : For using existing tools like wp-cli and process plugin update. 

+ 10% :If you have used terraform based cloud provisioning for hosting the app.

+ 10%: For Nginx configuration as a part of setup. 


You will get bonus points if you have added proper production logging in the queueing system with file rotate and hosted it on a publically accessible IP, use free credits in AWS, google cloud or digitalOcean .. PLEASE NOTE: This does not remove the requriement of submitting the code base.
